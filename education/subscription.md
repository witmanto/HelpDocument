# 訂閱地址 & 託管地址

{% hint style="danger" %}
閣下應當把訂閱地址和託管地址當做密碼一樣妥善保管，請勿泄露給他人！
{% endhint %}

{% hint style="info" %}
ShadowsocksR 使用「訂閱模式」是目前最推薦的做法，使用後令客戶端在每次啟動是自動嘗試更新配置文件，同時您也可以手動來立即更新節點訊息。
{% endhint %}

{% hint style="info" %}
Surge 使用「託管模式」是目前對小白最友好的方式，使用後令 Surge 在每 24 小時嘗試更新節點 & 規則，同時您也可以手動來立即更新節點訊息。
{% endhint %}

在 「1Mix」用戶中心首頁獲取

![](https://github.com/BrownRhined/HelpDocument/tree/603d2bd5d2643d4ceb2b12057796a97fc2d1df78/.gitbook/assets/meitu.jpg)

